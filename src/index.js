import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import "./css/index.css";
import Usuarios from "./components/Users/renders/Usuarios";
import * as serviceWorker from "./serviceWorker";
import Persona from "./components/Users/renders/Persona.jsx";
import Tareas from './components/Users/containers/Tareas.jsx';
import Albums from './components/Album/Albums.jsx';
import CommentsContainer from './components/Users/containers/CommentsContainer.jsx';
// import Posts from './Posts';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import Fotos from "./components/Album/Fotos";
import Home from "./components/Home/Home.jsx";
import CrearCuenta from "./components/Home/CrearCuenta";


const routes = (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/usuarios" component={Usuarios} />
      <Route exact path="/usuarios/:id" component={Persona} />
      <Route exact path="/todos/" component={Tareas} />
      <Route exact path="/albums" component={Albums} />
      <Route exact path="/albums/:id/photos" component={Fotos} />
      <Route exact path="/posts/:id/comments" component={CommentsContainer} />
      {/* <Route exact path="/posts/" component={Posts} /> */}
      <Route exact path="/create" component={CrearCuenta} />
    </Switch>
  </BrowserRouter>
);

ReactDOM.render(routes, document.getElementById("root"));

serviceWorker.unregister();
