import React from 'react';
import '../../css/home.css';
import Navbar from "../general/Navbar.jsx";
import { Link } from 'react-router-dom';


const HomeRender = props => {
	return(
		<div>
			<div className="h-100">
        <Navbar />
        <div className="d-flex flex-column justify-content-center align-items-center">
          <h1 className="title font-weight-bold">Bienvenidos a Sáltala!!</h1>
        </div>
        <div className="d-flex flex-column align-items-center centrado">
          <h2 className="login pb-5 font-weight-bold">Login</h2>
          <form className="d-flex flex-column align-items-center " onSubmit={props.handleFormSubmit}>
            <label htmlFor="usuario">Usuario</label>
            <input
              type="text"
              id="usuario"
              name="usuario"
              className=""
              value={props.usuarioInput}
              onChange={props.handleInput}
            />
            <label htmlFor="contraseña">Contraseña</label>
            <input
              type="password"
              id="contraseña"
              name="contraseña"
              className=""
              value={props.passwordInput}
              onChange={props.handleInput}
            />
            <button onClick={props.handleClick} className="btn btn-success m-2">Entrar</button>
          </form>
					<Link to="/create">Crear una cuenta</Link>
        </div>
      </div>
		</div>
	)
}

export default HomeRender;