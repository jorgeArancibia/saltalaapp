import React from "react";
import HomeRender from "./HomeRender.jsx";

class Home extends React.Component {
  state = {
    usuario: "",
    contraseña: ""
  };

  handleInput = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleFormSubmit = e => {
    e.preventDefault();

    const datosIn = {
      newUser: this.state.usuario,
      newPassword: this.state.contraseña
    };

    const localUser = JSON.parse(localStorage.getItem("newUser"));

    if (
      localUser.newUser === datosIn.newUser &&
      localUser.newPassword === datosIn.newPassword
    ) {
      console.log("Acceso concedido");
    } else {
      console.log("Acceso denegado");
    }
    console.log(datosIn);
    console.log(localUser);
  };

  render() {
    return (
      <>
        <HomeRender
          handleInput={this.handleInput}
          handleFormSubmit={this.handleFormSubmit}
          usuarioInput={this.state.usuario}
          contraseñaInput={this.state.contraseña}
        />
      </>
    );
  }
}

export default Home;
