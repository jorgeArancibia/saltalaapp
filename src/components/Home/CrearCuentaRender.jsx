import React from 'react';

const CrearCuentaRender = (props) => {
	return(
		<div>
			<div className="d-flex flex-column align-items-center justify-content-center pt-5">
          <h1>Crear cuenta</h1>
          <label htmlFor="newUser">Ingresa el nombre de tu cuenta</label>
					<input 
					className="col-3" 
					id="newUser"
          name="newUser"
					type="text"
					value={props.usuarioInput} //Para conectar el estado con el input en todo momento
          onChange={props.handleInput} 
					/>
          <label htmlFor="newPassword">Ingresa tu contraseña</label>
					<input 
					className="col-3" 
					id="newPassword"
          name="newPassword"
					type="text" 
					value={props.crearContraseñaInput}
        	onChange={props.handleInput}
					/>
					<button onClick={props.handleButtonCreate} className="btn btn-success m-2">Crear</button>
        </div>
		</div>
	)
}

export default CrearCuentaRender;