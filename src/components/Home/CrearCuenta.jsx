import React from "react";
import Navbar from "../general/Navbar.jsx";
import CrearCuentaRender from './CrearCuentaRender.jsx'

class CrearCuenta extends React.Component {
  state = {
    newUser: "",
    newPassword: ""
  };

  handleInput = e => {
    this.setState({ [e.target.name]: e.target.value }); // Identifica el input donde se está escribiendo, y lo setea en el estado
                                                        // que va a cambiar a través de la variable name del padre target del evento
                                                        // Por lo que se pueden setear muchos atributos en una sola linea de código.
  };

  handleButtonCreate = e => {
    e.preventDefault();

    const data = {
      newUser: this.state.newUser,
      newPassword: this.state.newPassword
    };

    if (localStorage.getItem("newUser")) { //Si localstorage tiene algo:
      const registrado = JSON.parse(localStorage.getItem("newUser")); // Se trae los usuarios registrados en string y los parsea.

      const validUser = registrado.filter(user => data.newUser === user.newUser); // Se va a filtrar si hay algún usuario con 
                                                                                  // el mismo nombre en el storage y va a 
                                                                                  //retornar ese usuario si lo encuentra(error, length=1)
                                                                                  //, sino,
                                                                                  //va a registrar el usuario ingresado en el storage
                                                                                  //(se retorna length=0 porque retorna vacio).

      if (validUser.length === 0) { // Si no retorna nada(va a retornar 0):
        registrado.push(data); // Se agrega el estado(lo que se esta ingresando) a lo que se trajo del storage.
        localStorage.setItem("newUser", JSON.stringify(registrado)); //Luego se setea en el storage.
      } else {
        alert("ERROR!, Usuario ya registrado, intenta registrarte con otro nombre.");
      }
    } else { // Si localStorage no tiene ningún usuario creado.
      const vacio = []; // Se crea un arreglo vacío.
      vacio.push(data); // Se ingresa en ese arreglo los datos que el usuario está ingresando.
      localStorage.setItem("newUser", JSON.stringify(vacio)); //Por último se setea ese arreglo en el storage.
    }
  };

  render() {
    return (
      <div>
        <Navbar />
        <CrearCuentaRender
          handleInput={this.handleInput}
          handleButtonCreate={this.handleButtonCreate}
          usuarioInput={this.state.newUser}
          crearContraseñaInput={this.state.newPassword}
        />
      </div>
    );
  }
}

export default CrearCuenta;
