import React from "react";
import DatosTareas from '../renders/DatosTareas.jsx';
import Loading from '../../general/Loading.jsx';
import Error from '../../general/Error.jsx';

class Tareas extends React.Component {
	state = {
		loading: true,
		tareas:[],
		error: false,
	}

	componentDidMount() {
		this.fetchTodos();
	}

	fetchTodos = async () => {
		try{
			const response = await fetch('https://jsonplaceholder.typicode.com/todos/');
			const tareas = await response.json();

			this.setState({
				loading:false,
				tareas
			})

		} catch (error) {
			this.setState({
				loading:false,
				error:true
			})
		}
	}


	render() {
		const { loading, error, tareas } = this.state;
		return(
			<div>
				{ loading ? (
					<Loading /> 
				) : error ? (
					<Error />
				) : (
				 <DatosTareas tareas={ tareas } />
				)}
			</div>
		)
	}
}

export default Tareas;