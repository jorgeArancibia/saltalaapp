import React from "react";
import Comments from "../renders/Comments.jsx";
import Loading from "../../general/Loading.jsx";
import Error from "../../general/Error.jsx";

class CommentsContainer extends React.Component {
  state = {
    loading: true,
    filas: [],
    error: false,
    columns: ["Nombre", "Email", "Comentario"]
  };

  componentDidMount() {
    this.fetchComments();
  }

  fetchComments = async () => {
    try {
      const response = await fetch(`https://jsonplaceholder.typicode.com/comments?postId=${this.props.match.params.id}`);

      const filas = await response.json();

      this.setState({
        loading: false,
        filas
      });
    } catch (error) {
      this.setState({
        loading: false,
        error: true
      });
    }
  };

  render() {
    const { loading, error, filas, columns } = this.state;
    return (
      <div>
        {loading ? (
          <Loading />
        ) : error ? (
          <Error />
        ) : (
          <Comments filas={filas} columns={columns} hasLink={false} />
        )}
      </div>
    );
  }
}

export default CommentsContainer;
