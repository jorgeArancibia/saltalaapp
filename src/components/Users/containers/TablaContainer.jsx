import React from "react";
import Tabla from "../../general/Tabla.jsx";
import Loading from "../../general/Loading.jsx";
import Error from "../../general/Error.jsx";

class TablaContainer extends React.Component {
  state = {
    loading: true,
    filas: [],
    error: false,
    message: "error de capa 8",
    columns: ["#", "name", "User Name"],
    link: "usuarios"
  };

  componentDidMount() {
    this.fetchPeople();
  }

  fetchPeople = async () => {
    try {
      this.setState({ loading: true });

      const response = await fetch(
        "https://jsonplaceholder.typicode.com/users"
      );
      const filas = await response.json();

      this.setState({
        loading: false,
        filas: filas
      });
    } catch (error) {
      this.setState({
        loading: false,
        error: true,
        message: error
      });
    }
  };

  render() {
    const { filas, loading, error } = this.state;
    return (
      <div>
        {loading ? (
          <Loading />
        ) : error ? (
          <Error />
        ) : (
          <Tabla
            filas={filas}
            columns={this.state.columns}
            hasLink={true}
            link={this.state.link}
          />
        )}
      </div>
    );
  }
}

export default TablaContainer;
