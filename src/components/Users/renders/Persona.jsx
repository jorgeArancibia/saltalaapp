import React from "react";
import Loading from "../../general/Loading.jsx";
import DatosPersona from "./DatosPersona.jsx";
import Error from "../../general/Error.jsx";
import axios from "axios";
import DatosUsuarioPosts from "./DatosUsuarioPosts.jsx";

class Persona extends React.Component {
  state = {
    loading: true,
    user: {},
    posts: [],
    error: false
  };

  componentDidMount() {
    this.fetchAllData();
  }

  // fetchData = async () => {
  //   try {
  //     this.setState({ loading: true });

  //     const response = await fetch(
  //       `https://jsonplaceholder.typicode.com/users/${
  //         this.props.match.params.id
  //       }`
  //     );
  //     const user = await response.json();

  //     if (user.id)
  //       this.setState({
  //         loading: false,
  //         user
  //       });
  //     else
  //       this.setState({
  //         loading: false,
  //         error: true,
  //         message: "no se encontro usuario"
  //       });
  //     console.log(this.state.user);
  //   } catch (error) {
  //     this.setState({
  //       loading: false,
  //       error: true
  //     });
  //   }
  // };

  fetchAllData = async () => {
    this.setState({ loading: true });

    const [user, posts] = await Promise.all([
      axios.get(
        `https://jsonplaceholder.typicode.com/users/${this.props.match.params.id}`
      ),
      axios.get(
        `https://jsonplaceholder.typicode.com/posts?userId=${this.props.match.params.id}`
      )
    ]);

    const { data: dataUser } = user;
    const { data: dataPosts } = posts;
    this.setState({ loading: false, user: dataUser, posts: dataPosts });
  };

  render() {
    return (
      <div>
        {this.state.loading ? (
          <Loading />
        ) : this.state.error ? (
          <Error />
        ) : (
          <>
            <DatosPersona user={this.state.user} />
            <DatosUsuarioPosts posts={this.state.posts} />
          </>
        )}
      </div>
    );
  }
}

export default Persona;
