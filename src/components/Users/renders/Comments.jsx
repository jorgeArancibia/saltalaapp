import React from "react";
import Navbar from "../../general/Navbar.jsx";
import Tabla from "../../general/Tabla.jsx";

const Comments = ({filas, columns, hasLink}) => {
  return (
    <div>
      <Navbar />
      <Tabla filas={filas} columns={columns} hasLink={hasLink}/>
    </div>

    // <div>
    //   <Navbar />
    //   <table className="table">
    //     <thead>
    //       <tr>
    //         <th>#</th>
    //         <th>Título del comentario</th>
    //         <th>Email</th>
    //         <th>Comentario</th>
    //       </tr>
    //     </thead>
    //     <tbody>
    //       {props.comments.map(comment => {
    //         return (
    //           <tr key={comment.id}>
    //             <td>{comment.postId}</td>
    //             <td>{comment.name}</td>
    //             <td>{comment.email}</td>
    //             <td>{comment.body}</td>
    //           </tr>
    //         );
    //       })}
    //     </tbody>
    //   </table>
    // </div>
  );
};

export default Comments;
