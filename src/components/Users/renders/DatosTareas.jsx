import React from "react";
import Navbar from "../../general/Navbar.jsx"

const DatosTareas = props => {
  return (
    <div>
      <Navbar />
      <table className="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Título</th>
            <th>Estado</th>
          </tr>
        </thead>
        <tbody>
          { props.tareas.map( tarea => {
            return (
              <tr key={tarea.id}>
                <td>{tarea.userId}</td>
                <td>{tarea.title}</td>
                <td>
                  <div className="form-check">
                    <input type="checkbox" className="form-check-input" defaultChecked={tarea.completed} disabled />
                  </div>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default DatosTareas;
