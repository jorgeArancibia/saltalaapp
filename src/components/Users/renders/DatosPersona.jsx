import React from "react";

const Tabla = props => {
	const { name, username, email, address, phone, website, company} = props.user;
	return (
		<>
			<h2 className="bg-primary pl-2">Datos</h2>
			<table className="table">
				<thead>
				</thead>
				<tbody>
					<tr>
						<td>Name:</td>
						<td>{name}</td>
					</tr>
					<tr>
						<td>User Name:</td>
						<td>{username}</td>
					</tr>
					<tr>
						<td>Email:</td>
						<td>{email}</td>
					</tr>
					<tr>
						<td>Dirección:</td>
						<td>{address.suite}, {address.street}, {address.city}</td>
					</tr>
					<tr>
						<td>Phone:</td>
						<td>{phone}</td>
					</tr>
					<tr>
						<td>Website:</td>
						<td>{website}</td>
					</tr>
					<tr>
						<td>Compañia:</td>
						<td>{company.name}</td>
					</tr>
				</tbody>
			</table>
		</>
  );
};

export default Tabla;
