import React from 'react';
import TablaContainer from '../containers/TablaContainer.jsx';
import Navbar from '../../general/Navbar.jsx';

const Usuarios = () => {
  return (
    <div>
      <Navbar />
      <TablaContainer />
    </div>
  );
}

export default Usuarios;
