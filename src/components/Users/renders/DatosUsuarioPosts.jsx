import React from 'react';
import { Link } from 'react-router-dom';

const DatosUsuarioPosts = (props) => {
	return(
		<>
		<h2 className="bg-primary rounded pl-2">Posts</h2>
		<table className="table">
			<thead>
				<th>#</th>
				<th>Nombre del Post</th>
				<th>Post</th>
			</thead>
			<tbody>
				{props.posts.map( post => {
					return(
						<tr key={post.id}>
							<td><Link to={`/posts/${post.id}/comments`}>{post.id}</Link></td>
							<td>{post.title}</td>
							<td>{post.body}</td>
						</tr>
					)
				})}
			</tbody>
		</table>
		</>
	)
}

export default DatosUsuarioPosts;