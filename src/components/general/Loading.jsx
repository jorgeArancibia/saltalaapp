import React from "react";
import logo from '../../images/logo.png';
import '../../css/loading.css'

const Loading = () => {
  return (
      <div className="d-flex justify-content-center align-items-center vh-100">
        <img className="rotacion" src={logo} alt="logo" />
      </div>
  );
};

export default Loading;