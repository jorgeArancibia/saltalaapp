import React from "react";
import { Link } from "react-router-dom";

const Tabla = ({columns, filas, hasLink, link}) => {
  return (
    <table className="table">
      <thead>
        <tr>
          {columns.map((column, index) => (
            <th key={index}>{column}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {filas.map(fila => {
          return (
            <tr key={fila.id}>
              <td>
                {hasLink ? (
                  <Link to={`/${link}/${fila.id}`}>{fila.id}</Link>
                ) : (
                  fila.id
                )}
              </td>
              <td>{fila.name}</td>
              <td>{fila.username}</td>
              <td>{fila.email}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default Tabla;
