import React from "react";
import { Link } from "react-router-dom";
import banner from "../../images/banner2.png"


const Navbar = () => {
	return(
		<>
			<nav className="navbar navbar-expand-lg navbar-light bg-light d-flex">
				<div className="mr-auto p-2">
					<img  src={banner} alt="banner"/>
				</div>
			<div className="d-flex justify-content-end">
				<Link to={`/`} className="navbar-brand" >Home</Link>
				<Link to={`/usuarios/`} className="navbar-brand" >Users</Link>
				<Link to={`/todos/`} className="navbar-brand">Todos</Link>
				<Link to={`/albums/`} className="navbar-brand">Albums</Link>
				{/* <Link to={`/comments/`} className="navbar-brand">Comments</Link> */}
				</div>
			</nav>
		</>
	)
}

export default Navbar;