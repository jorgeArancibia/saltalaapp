import React from "react";

const Error = () => {
	return(
		<div className="container">
			<div className=" d-flex align-items-center vh-100">
				<h1 className="alert alert-danger" >ERROR: No se puede mostrar la página porque no se han encontrado el archivo</h1>
			</div>
		</div>
	)
}

export default Error;