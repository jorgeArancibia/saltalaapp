import React from "react";

const DatosFotosAlbum = props => {
  return (
    <>
      <h1 className="p-4">Imagenes</h1>
      <div className="d-flex flex-wrap">
        {props.fotos.map( fotos => {
          return(
            <div className="card col-2">
              <img src={fotos.url} className="card-img-top" alt="imagen"/>
              <div className="card-body">
                <h5 className="card-title font-weight-bold">{fotos.title}</h5>
              </div>
            </div>
          )
        })}
      </div>
    </>
  );
};

export default DatosFotosAlbum;
