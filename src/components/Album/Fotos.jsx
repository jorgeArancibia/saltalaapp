import React from "react";
import Loading from "../general/Loading.jsx";
import Error from "../general/Error.jsx";
import DatosFotosAlbum from "./DatosFotosAlbum.jsx";
import axios from "axios"

class Fotos extends React.Component {
  state = {
    loading: true,
    fotos: [],
    error: false,
    message: "Error de Capa 8"
  };

  componentDidMount() {
    this.fetchFotos();
  }

  fetchFotos = async () => {
    try {
			const { data } = await axios.get(`https://jsonplaceholder.typicode.com/photos?albumId=${this.props.match.params.id}`);

      this.setState({
        loading: false,
        fotos: data
      });
    } catch (error) {
      this.setState({
        loading: false,
        error: true
      });
    }
  };

  render() {
    const { loading, error, fotos } = this.state;
    return (
      <div>
        {loading ? (
          <Loading />
        ) : error ? (
          <Error />
        ) : (
          <DatosFotosAlbum fotos={fotos} />
        )}
      </div>
    );
  }
}

export default Fotos;
