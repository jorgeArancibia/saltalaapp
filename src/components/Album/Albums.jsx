import React from "react";
import DatosAlbums from'./DatosAlbums.jsx';
import Loading from '../general/Loading.jsx';
import Error from '../general/Error.jsx';
import axios from "axios";

class Albums extends React.Component {
	state = {
		loading: true,
		albums: [],
		error: false,
		message: 'Error de capa 8'
	}

	componentDidMount(){
		this.fetchAlbums();
	}

	fetchAlbums = async () => {
		try {
			const { data } = await axios.get(`https://jsonplaceholder.typicode.com/albums/`);

			this.setState({
				loading: false,
				albums: data
			})

		} catch (error) {
			this.setState({
				loading: false,
				error: true
			})
		}
	}

	render(){
		const { loading, error, albums } = this.state
		return(
			<div>
				{ loading ? (
					<Loading />
				) : error ? (
					<Error />
				) : (
					<DatosAlbums albums={ albums } />
				) }
			</div>
		)
	}
}

export default Albums;