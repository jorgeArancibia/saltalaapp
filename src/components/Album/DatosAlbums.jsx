import React from "react";
import Navbar from '../general/Navbar.jsx';
import { Link } from 'react-router-dom';

const DatosAlbums = props => {
	return(
		<div>
			<Navbar />
			<table className="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Nombre de Album</th>
					</tr>
				</thead>
				<tbody>
					{ props.albums.map( album => {
						return(
							<tr key={ album.id }>
								<td><Link to={`/albums/${album.id}/photos`}>{album.id}</Link></td>
								<td>{ album.title }</td>
							</tr>
						)
					})}
				</tbody>
			</table>
		</div>
	)
}

export default DatosAlbums;